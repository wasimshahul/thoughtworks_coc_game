package COC;

import java.util.ArrayList;

class Game {

    private int maxRounds;
    private ArrayList<Player> players = new ArrayList<>();
    public static int currentRound = 0;

    public int getMaxRounds() {
        return maxRounds;
    }

    public void setMaxRounds(int maxRounds) {
        this.maxRounds = maxRounds;
    }

    void addPlayer(Player player) {
        players.add(player);
    }

    public int getCurrentRound(){
        return currentRound;
    }

    public Player getWinner(){
        if(players.get(0).getCurrentScore()>players.get(1).getCurrentScore()){
            System.out.println("Winner is Player1");
            return players.get(1);
        } else {
            System.out.println("Winner is Player2");
            return players.get(0);
        }
    }

    void start() {
        for (int i=1; i <= getMaxRounds(); i++){
            currentRound = i;
            int player1Choice = players.get(0).choice();
            int player2Choice = players.get(1).choice();

            System.out.println("------------------------- Round "+i+"-------------------------");
            System.out.println("Player 1 Choice : "+player1Choice);
            System.out.println("Player 2 Choice : "+player2Choice);

            int[] currentRoundScore = Score.calculate(player1Choice, player2Choice);
            players.get(0).setCurrentScore(currentRoundScore[0]);
            players.get(1).setCurrentScore(currentRoundScore[1]);
            System.out.println("------------------------- Round "+i+" Score-------------------------");
            System.out.println("Player 1 score updated to : "+players.get(0).getCurrentScore());
            System.out.println("Player 2 score updated to : "+players.get(1).getCurrentScore());

            if(i==getMaxRounds()){
                getWinner();
            }
        }
    }
}

package COC;

public class Constants {
    public static final int CHEAT = 0;
    public static final int COOPERATE = 1;
    public static final int ACTUAL_PLAYER = 2;
    public static final int KIND_BOT = 3;
    public static final int EVIL_BOT = 4;
    public static final int COPYCAT_BOT = 5;
    public static final int GRUDGER_BOT = 6;
}

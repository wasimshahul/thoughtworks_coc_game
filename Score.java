package COC;

import java.util.ArrayList;

public class Score {

    public ArrayList<Integer> getScores() {
        return scores;
    }

    ArrayList<Integer> scores = new ArrayList<>();

    public static int[] calculate(int choice1, int choice2) {
        int[] score = new int[2];

        if(choice1 == Constants.COOPERATE && choice2 == Constants.COOPERATE){
            score[0] = 2;
            score[1] = 2;
        } else if (choice1 == Constants.CHEAT && choice2 == Constants.CHEAT){
            score[0] = 0;
            score[1] = 0;
        } else if (choice1 == Constants.COOPERATE && choice2 == Constants.CHEAT){
            score[0] = -1;
            score[1] = 3;
        } else if (choice1 == Constants.CHEAT && choice2 == Constants.COOPERATE){
            score[0] = 3;
            score[1] = -1;
        }
        return score;
    }

}

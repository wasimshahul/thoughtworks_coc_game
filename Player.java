package COC;

import java.util.Random;

public class Player {

    int currentScore = 0;
    int choice;
    Score opponentScore, myScore;
    int playerType;

    public Player(int playerType, Score myScore){
        this.playerType = playerType;
        this.myScore = myScore;
    }

    public Player(int playerType, Player player2, Game game){
        this.playerType = playerType;
    }

    public Player(int playerType, Score opponentScore, Score myScore){
        this.playerType = playerType;
        this.opponentScore = opponentScore;
        this.myScore = myScore;
    }

    public int choice() {
        if(this.playerType == Constants.ACTUAL_PLAYER){
            int choice = new PlayerBehaviour(Constants.ACTUAL_PLAYER).getChoice();
            myScore.getScores().add(choice);
            return choice;
        } else if(this.playerType == Constants.KIND_BOT){
            int choice = new PlayerBehaviour(Constants.KIND_BOT).getChoice();
            myScore.getScores().add(choice);
            return choice;
        } else if(this.playerType == Constants.EVIL_BOT){
            int choice = new PlayerBehaviour(Constants.EVIL_BOT).getChoice();
            myScore.getScores().add(choice);
            return choice;
        } else if(this.playerType == Constants.COPYCAT_BOT){
//            System.out.println("Sending current round value as : "+game.getCurrentRound());
            int choice = new PlayerBehaviour(Constants.COPYCAT_BOT, opponentScore, Game.currentRound).getChoice();
            myScore.getScores().add(choice);
            return choice;
        } else if(this.playerType == Constants.GRUDGER_BOT){
            int choice = new PlayerBehaviour(Constants.GRUDGER_BOT, opponentScore, Game.currentRound).getChoice();
            myScore.getScores().add(choice);
            return choice;
        } else {
            int choice = new Random().nextInt(2);
            myScore.getScores().add(choice);
            return choice;
        }

    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = this.currentScore + currentScore;
    }

    public int getCurrentScore() {
        return this.currentScore;
    }
}

package COC;


import static COC.Constants.*;

public class Main {

    public static void main(String[] args) {

        Player kindBot, evilBot, copyCatBot, grudgerBot;

        //Match 1
        Game game1 = new Game();
        Score player1Score = new Score();
        Score player2Score = new Score();
        kindBot = new Player(KIND_BOT, player1Score);
        copyCatBot = new Player(COPYCAT_BOT, player1Score, player2Score);
        game1.setMaxRounds(5);
        game1.addPlayer(kindBot);
        game1.addPlayer(copyCatBot);
        game1.start();

        //Match 2
        Game game2 = new Game();
        evilBot = new Player(EVIL_BOT, player1Score);
        game2.setMaxRounds(5);
        game2.addPlayer(evilBot);
        game2.addPlayer(game1.getWinner());
        game2.start();

        //Match 3
        Game game3 = new Game();
        kindBot = new Player(KIND_BOT, player1Score);
        game3.setMaxRounds(5);
        game3.addPlayer(kindBot);
        game3.addPlayer(game2.getWinner());
        game3.start();

        //Match 4
        Game game4 = new Game();
        game4.setMaxRounds(5);
        game4.addPlayer(game2.getWinner());
        game4.addPlayer(game3.getWinner());
        game4.start();

        System.out.println(game4.getWinner()+"");
    }
}

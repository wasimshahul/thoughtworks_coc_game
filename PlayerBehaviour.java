package COC;

import java.util.Random;

public class PlayerBehaviour {

    int type, gameRoundIndex;
    Score opponentScore;

    public PlayerBehaviour(int type){
        this.type = type;
    }

    public PlayerBehaviour(int type, Score opponentScore, int gameRoundIndex){
        this.type = type;
        this.opponentScore = opponentScore;
        this.gameRoundIndex = gameRoundIndex;
    }

    public int getType() {
        return type;
    }

    public int getChoice() {
        if(getType() == Constants.ACTUAL_PLAYER){
            return new Random().nextInt(2);
        }else if(getType() == Constants.KIND_BOT){
            return (Constants.COOPERATE);
        } else if(getType() == Constants.EVIL_BOT){
            return (Constants.CHEAT);
        } else if(getType() == Constants.COPYCAT_BOT){
            if(gameRoundIndex == 1){
                return Constants.COOPERATE;
            } else {
                int prevChoiceOfOpponent = opponentScore.getScores().get(gameRoundIndex-2);
                return prevChoiceOfOpponent;
            }
        }else if(getType() == Constants.GRUDGER_BOT){
            boolean hasCheated = false;
            for(int i = 0 ; i< gameRoundIndex-1 ; i++){
                if((opponentScore.getScores().get(i).equals(Constants.CHEAT))){
                    hasCheated = true;
                }
            }
            if(hasCheated){
                return Constants.CHEAT;
            } else {
                return Constants.COOPERATE;
            }
        } else {
            return (new Random().nextInt(2));
        }
    }
}
